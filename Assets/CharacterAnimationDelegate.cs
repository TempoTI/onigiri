﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationDelegate : MonoBehaviour
{
    [SerializeField] GameObject clawsCollider_R;
    [SerializeField] GameObject clawsCollider_L;

   void ClawsCollider_R_On()
    {
        clawsCollider_R.SetActive(true);
    }
    void ClawsCollider_R_Off()
    {
        if (clawsCollider_R.activeInHierarchy)
        {
            clawsCollider_R.SetActive(false);
        }
    }

    void ClawsCollider_L_On()
    {
        clawsCollider_L.SetActive(true);
    }
	void ClawsCollider_L_Off()
	{
		if (clawsCollider_L.activeInHierarchy)
		{
			clawsCollider_L.SetActive(false);
		}
	}
    // Update is called once per frame
    void Update()
    {
        
    }
}
