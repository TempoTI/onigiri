﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalAttack : MonoBehaviour
{
    public LayerMask collisionLayer;
    [SerializeField]float radius = .5f;
    [SerializeField]float damage =2f;
    [SerializeField] GameObject hit_fx;
    [SerializeField] bool isPlayer, isEnemy;
    [SerializeField] CharacterStatus character;
    // Start is called before the first frame update
    void Start()
    {
        character = gameObject.GetComponentInParent<CharacterStatus>();
    }

    // Update is called once per frame
    void Update()
    {
        DetectCollision();
    }

    private void DetectCollision()
    {
        //detect collisions
        Collider[] hit = Physics.OverlapSphere(transform.position, radius, collisionLayer);

        if(hit.Length > 0)
        {

            Debug.Log("i've the "+hit[0].gameObject.name);

            gameObject.SetActive(false);
            if (isPlayer)
            {
			
				float dmgAmount =character.GetDamage();

                hit[0].GetComponent<CharacterStatus>().TakeDamage(dmgAmount);
				
                Vector3 hitFX_pos = hit[0].transform.position;
                
            }


        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(gameObject.transform.position, radius);
        Gizmos.color=Color.yellow;
    }
}
