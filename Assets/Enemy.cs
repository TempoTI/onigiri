﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : CharacterStatus
{
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }
   
    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void Die()
    {
        Destroy(gameObject);
    }
}
