﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStatus : MonoBehaviour
{
	[Header("Status")]
	[SerializeField] protected float maxHealth;
	[SerializeField] protected float currentHealth;
	[SerializeField] protected float dmg;
	[SerializeField] protected float moveSpeed = 10;


    protected virtual void Die() {
        
    }
    // Start is called before the first frame update
    protected virtual void Start()
    {
		currentHealth = maxHealth;
    }

	public float GetDamage(){
		return dmg;
	}

	//take damage
	public void TakeDamage(float dmg)
	{
       
		currentHealth -= dmg;
        Debug.Log("me: "+gameObject.name+"took "+dmg+" dmg and now i'm at:"+currentHealth);

        if (currentHealth <=0){
			Die();
		}
	}

	

	//get life as percentage
	public float GetLifeAsPercentage ()
	{
		float lifeAsPercentage = currentHealth / maxHealth;
		return lifeAsPercentage;
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
