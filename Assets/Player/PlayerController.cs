﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : CharacterStatus
{
   [Header("Movement and physics")]
    Vector3 moveDirection;
    CharacterController playerController;
    float gravity;
    Rigidbody rigidBody;
    public GameObject playerGFX;
    [SerializeField] float jumpForce = 10;
    [SerializeField] float defaultGravity = 15;
    [SerializeField] float glideGravity = 5;
    [SerializeField] float glideTimeCounter;
    [SerializeField] float glideTime;




    [Header("Status")]
    [SerializeField] bool isInvunerable = false;
    [SerializeField] float invunerabilityTime = 0.5f;
    [SerializeField] float knockbackCounter;
    [SerializeField] float knockbackForce=1;
    [SerializeField] float knockbackTime = 0.3f;

    [Header("Animation")]
    [SerializeField] Animator playerAnim;
    AnimationEvents AnimEvents;

    void Start()
    { 
        playerController = GetComponent<CharacterController>();
        gravity = defaultGravity;
        glideTimeCounter = glideTime;
        playerAnim = GetComponentInChildren<Animator>();
        AnimEvents = GetComponentInChildren<AnimationEvents>();
        rigidBody = GetComponent<Rigidbody>();
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        ApplyGravity();
        GetInput();
        ProcessMovement();
        RotateModel();
        
    }


    private void RotateModel()
    {
        if (moveDirection.x > 0)
        {
            playerGFX.transform.localScale=new Vector3(1,1,1);
        }
        else if ( moveDirection.x < 0)
        {
            playerGFX.transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    private void ApplyGravity()
    {
        moveDirection.y = moveDirection.y - gravity * Time.deltaTime;
        if (playerController.isGrounded)
        {
            gravity = defaultGravity;
            glideTimeCounter = glideTime;
        }
    }

    private void ProcessMovement()
    {
        if (knockbackCounter <= 0)
        {
            playerController.Move(moveDirection * Time.deltaTime);
        }
        else
        {     
            knockbackCounter -= Time.deltaTime;
        }
    }

    

        private void GetInput()
        {
            //Move
            moveDirection.x = Input.GetAxis("Horizontal") * moveSpeed;
            //Jump
            if (playerController.isGrounded)
            {

                if (Input.GetButtonDown("Jump"))
                {
                    playerAnim.SetBool("inAir", true);
                    moveDirection.y = jumpForce * Time.deltaTime;
                }
                else
                {
                    playerAnim.SetBool("inAir", false);
                    playerAnim.SetFloat("Speed", Mathf.Abs(moveDirection.x));
                }

            }

            //Glide
            if (Input.GetButton("Jump") && !playerController.isGrounded)
            {

                playerAnim.SetBool("isGliding", true);
                gravity = glideGravity;

                glideTimeCounter -= Time.deltaTime;

            }
            if (glideTimeCounter <= 0 || Input.GetButtonUp("Jump") || playerController.isGrounded)
            {

                playerAnim.SetBool("isGliding", false);

                gravity = defaultGravity;
            }



            //Attack(skretch)
            if (Input.GetButtonDown("Attack") && playerController.isGrounded)
            {
                if (AnimEvents.canclick)
                {
                    AnimEvents.noOfClicks++;
                }
                if (AnimEvents.noOfClicks == 1)
                {
                    playerAnim.SetInteger("attack", 1);

                }

            }
            if (Input.GetButton("Attack")&& !playerController.isGrounded)
        {
            playerAnim.SetTrigger("Attacking");
        }




            //FlameThrower
            if (Input.GetButtonDown("Fire") && playerController.isGrounded)
            {
                playerAnim.SetTrigger("FlameThrow");
                FlameThrower();
            }
            
        }

    void FlameThrower()
    {
       //TODO spwan particles
       //TODO make sothing to deal dmg
    }

    // combat stuff


    // TODO show and update life in UI

 

     //detecting damage
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "Enemy" && !isInvunerable)
        {
            CharacterStatus enemy = hit.gameObject.GetComponent<CharacterStatus>();
            float dmg = enemy.GetDamage();
            TakeDamage(dmg);
            //get direction of knockback
            Vector3 knockbackDirection = hit.transform.position - transform.position;
            knockbackDirection = knockbackDirection.normalized;
            //TODO apply knockback
            ApplyKnockback(knockbackDirection);
            //TODO get a few frames of invunerable
            StartCoroutine(GetInvunerable(invunerabilityTime));
           

        }
    }

    private void ApplyKnockback(Vector3 direction)
    {
        playerAnim.SetTrigger("Hurt");
        knockbackCounter = knockbackTime;
        moveDirection = direction *1.5f;
        moveDirection = new Vector3(-moveDirection.x, 1, 0);
        playerController.Move(moveDirection);
    }

    //make you invunerable
    private IEnumerator GetInvunerable(float time)
    {
        
        isInvunerable = true;
        yield return new WaitForSeconds(time);
        isInvunerable = false;
    }
   
	//Die
	protected override void Die(){
		// restart level
	
	}
}