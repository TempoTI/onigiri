﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
    public bool canclick = true;
    public int noOfClicks = 0;
    [SerializeField] Animator playerAnim;

    private void Start()
    {
        playerAnim = GetComponent<Animator>();
    }

    public void ComboCheck()
    {      
        canclick = false;
        if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attack 1") && noOfClicks == 1)
        {
            playerAnim.SetInteger("attack", 0); 
            canclick = true;
            noOfClicks = 0;
        }
        else if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attack 1") && noOfClicks >= 2)
        {
            playerAnim.SetInteger("attack", 2);
            canclick = true;
        }
        else if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attack 2") && noOfClicks == 2)
        {
     
            playerAnim.SetInteger("attack", 0);
            canclick = true;
            noOfClicks = 0;
        }
        else if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attack 2") && noOfClicks >= 3)
        {
       
            playerAnim.SetInteger("attack", 3);
            canclick = true;
           
        }
        else if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attack 3"))
        {

            playerAnim.SetInteger("attack", 0);  
            canclick = true;
            noOfClicks = 0;

        }

  
        

    }
}
